/**
 * AJAX call for autocomplete 
 * https://app.swaggerhub.com/apis-docs/RINGGOLD-INC/RinggoldIdentifyRestApi/2.6
 * https://support.ringgold.com/identify-database-api-services
 * author: bill.haase@ringgold.com
 * version: 0.1.1
 */

$(document).ready(function(){
  // replace with your API key from Ringgold
  var apikey = 'aaaa';
  var apiurl = "https://api.ringgold.com"
      + "/rest/v26/identify/organizations/search";
  $("#search-box").keyup(function(){
		$.ajax({
		type: "GET",
		url: apiurl,
		data:'mode=name'  // search names
      + '&out=2'    // detailed objects
      + '&limit=5'  // limit number of results
      + '&key=' + apikey 
      + '&q='+$(this).val(),  // search value from #search-box
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
      var tmp = [];
      // format search results for display.
      var srcData = data['data'];
      srcData.forEach( function(item) {
        // format each display result from fields in the data[] array.
        tmp.push(item['ringgold_id'] + ':' + item['name'] + "<br />");
      });
      
  		$("#suggestion-box").show();
			$("#suggestion-box").html(tmp);
			$("#search-box").css("background","#FFF");
		}
		});
	});
});