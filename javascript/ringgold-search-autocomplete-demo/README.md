# Ringgold Search Autocomplete demo
A Ringgold.com demonstration (javascript) to autocomplete a search using our API.

Contact support@ringgold.com with questions. 

The demonstration code is in folder 'dist'.
* Modify dist/script.js and replace the apikey='aaaa' with apikey='your_apikey_from_ringgold'.
* open dist/index.html to test.